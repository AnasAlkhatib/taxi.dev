<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Push Notifications   Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during sending push notifications 
    | to devices.
    |
    */

    'farecalc' => "Fare is calculated based on distance and car type plus initial price, the formula for calculation is : Distance in KM  * (Car Base Price+Initial Price) + Initial Meter Price . Each Car has it's own Base Price",


    'faredetails' => "the total fare is based on  distance(km) *(car base price+ride base price) (:distance)*(:carbaseprice+:initprice)",
    'Taxi'        => ' 
                       Base Price For Taxi',
    'Van'         => ' 
                       Base Price For Van',
    'Car'         => ' 
                       Base Price For Economy Car',

    'baseprice'   => ' 
                       Base Price :',  

    'init_meter'   => ' 
                       Initial Meter :',

];

