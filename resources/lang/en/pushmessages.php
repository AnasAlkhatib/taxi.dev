<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Push Notifications   Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during sending push notifications 
    | to devices.
    |
    */

    'nodrivers'   => 'No Drivers Near By',


];
