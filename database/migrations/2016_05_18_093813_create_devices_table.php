<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('registraion_id', 255);
            $table->enum('os', ['IOS', 'Android']);
            $table->string('os_version', 64);
            $table->string('app_version',10);
            $table->enum('app_type', ['Rider', 'Driver']);
            $table->string('device_model');
            $table->decimal('location_lat', 10, 8);
            $table->decimal('location_lon', 11, 8);
            $table->timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
