<?php

return array(

    'RiderAppIOS'     => array(
        'environment' =>'development',
        'certificate' =>env('IOS_PUSH_KEYS').'rider/key.pem',
        'passPhrase'  =>env('IOS_PUSH_SECRET'),
        'service'     =>'apns'
    ),
    'DriverAppIOS'     => array(
        'environment' =>'development',
        'certificate' =>env('IOS_PUSH_KEYS').'driver/key.pem',
        'passPhrase'  =>env('IOS_PUSH_SECRET'),
        'service'     =>'apns'
    ),
    'RiderAndroid' => array(
        'environment' =>'development',
        'apiKey'      =>env('ANDROID_RIDER_PUSH_KEY'),
        'service'     =>'gcm'
    ),
    'DriverAndroid' => array(
        'environment' =>'development',
        'apiKey'      =>env('ANDROID_DRIVER_PUSH_KEY'),
        'service'     =>'gcm'
    )

);