<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
  ],
  'apn' => [
      'priority' => 'normal',
      'certificate' => __DIR__ . '/iosCertificates/iHRCER.pem',
      'passPhrase' => '123456', //Optional
      'passFile' =>'', //Optional
      'dry_run' => false
  ]
];


 
