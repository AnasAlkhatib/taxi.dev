# Maued App #

### Installation ###


```sh


git clone git@bitbucket.org:AnasAlkhatib/taxi.dev.git

```


```sh

composer install
```


```sh

cp .env.example .env
```


```sh


php artisan key:generate && php artisan jwt:secret 
```


```sh

php artisan migrate
```

```sh

php artisan db:seed --class="Sto\Services\Core\Seeders\DatabaseSeeder"
```


* Configure your host
* Navigate to http://maued.dev/api/docs for swagger docs.


 
