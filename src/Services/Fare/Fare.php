<?php


namespace Sto\Services\Fare;

use Sto\Services\GeoTools\GeoTools;

class Fare
{
    public static $mode;

    public static function fareCalculator($distance, $meter = 1)
    {
        return 0.30 + ($distance * $meter);
    }
}