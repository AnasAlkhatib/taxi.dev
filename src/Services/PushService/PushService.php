<?php

namespace Sto\Services\PushService;

use Sto\Modules\Device\Repositories\Eloquent\DeviceRepository;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Log;

class PushService
{
    public static function pushMultipleMessage($userlist, $message)
    {

        for ($i = 0; $i < $userlist->count(); $i++) {
            if ($userlist[$i]['os'] == 'Android' && !empty($userlist[$i]['device_token']))
                PushService::pushSingleMessage($userlist[$i]['device_token'], $message, 'DriverAndroid');
            else if ($userlist[$i]['os'] == 'IOS' && !empty($userlist[$i]['device_token']))
                PushService::pushSingleMessage($userlist[$i]['device_token'], $message, 'DriverAppIOS');
        }

    }

    public static function pushSingleMessage($token, $message, $app)
    {

        return PushNotification::app($app)
            ->to($token)
            ->send($message);
    }

    public static function setCustomMessage($user, $ride_id, $message_text, $notification_type)
    {
        return $message = PushNotification::Message(
            $message_text,
            [
                'custom' =>
                    ['custom data' =>
                        [
                            'notitype' => $notification_type,
                            'rider_id' => $user->id,
                            'ride_id' => $ride_id,
                            'rider_name' => $user->name,
                            'rider_mobile_number' => $user->phone_number,
                            'rider_location' => [
                                'latitude' => $user->latitude,
                                'longitude' => $user->longitude
                            ]
                        ]

                    ]
            ]
        );

    }

    public static function setAcceptCustomMessage($rider_id, $ride_id, $arrival_time, $driver_name, $driver_mobile, $message_text, $notification_type)
    {


        return $message = PushNotification::Message(
            $message_text,
            [
                'custom' =>
                    ['custom data' =>
                        [
                            'notitype' => $notification_type,
                            'rider_id' => $rider_id,
                            'ride_id' => $ride_id,
                            'arrival_time' => $arrival_time,
                            'driver_name' => $driver_name,
                            'driver_mobile_number' => $driver_mobile
                        ]
                    ]
            ]
        );

    }

    public static function setCancelMessage($ride_id, $message_text, $notification_type)
    {
        return $message = PushNotification::Message(
            $message_text,
            [
                'custom' =>
                    ['custom data' =>
                        [
                            'notitype' => $notification_type,
                            'ride_id' => $ride_id
                        ]

                    ]
            ]
        );

    }
}