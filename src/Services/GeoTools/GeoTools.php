<?php

namespace Sto\Services\GeoTools;


class GeoTools
{
    public static function getDistance($a_latitude, $a_longitude, $b_latitude, $b_longitude)
    {
        $coordA   = \Toin0u\Geotools\Facade\Geotools::coordinate([$a_latitude, $a_longitude]);
        $coordB   = \Toin0u\Geotools\Facade\Geotools::coordinate([$b_latitude, $b_longitude]);

        $distance = \Toin0u\Geotools\Facade\Geotools::distance()->setFrom($coordA)->setTo($coordB);

        return $distance->in('km')->haversine();
    }

    public static function getEta($a_latitude, $a_longitude, $b_latitude, $b_longitude, $speed=45)
    {
        return self::getDistance($a_latitude, $a_longitude, $b_latitude, $b_longitude) / $speed * 60;
    }

    public static function getEtaByDistance($distance, $speed=45)
    {
        return ($distance / $speed) * 60;
    }
}