<?php

namespace Sto\Services\Authentication\Portals;

/**
 * Class TokenTrait.
 *
 * To be `used` by the `User` Model.
 *
 * 
 */
trait TokenTrait
{

    /**
     * inject a token in the user model itself.
     * if no token provided generate token first.
     *
     * @param null $token
     *
     * @return $this
     */
    public function injectToken($token)
    {
        // attach the token on the user
        $this->token = $token;

        return $this;
    }
}
