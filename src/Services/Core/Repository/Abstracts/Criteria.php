<?php

namespace Sto\Services\Core\Repository\Abstracts;

use Prettus\Repository\Contracts\CriteriaInterface as PrettusCriteriaInterface;

/**
 * Class Criteria.
 *
 * 
 */
abstract class Criteria implements PrettusCriteriaInterface
{

}
