<?php

namespace Sto\Services\Core\Controller\Abstracts;

use Sto\Http\Controllers\Controller as LaravelController;

/**
 * Class MasterController.
 *
 * 
 */
abstract class MasterController extends LaravelController
{

}
