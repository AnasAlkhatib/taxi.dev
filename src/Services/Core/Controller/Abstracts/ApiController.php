<?php

namespace Sto\Services\Core\Controller\Abstracts;

use Dingo\Api\Routing\Helpers as DingoApiHelper;
use Sto\Services\Core\Controller\Contracts\ApiControllerInterface;

/**
 * Class ApiController.
 *
 * 
 */
abstract class ApiController extends MasterController implements ApiControllerInterface
{

    use DingoApiHelper;
}
