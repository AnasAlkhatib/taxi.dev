<?php

namespace Sto\Services\Core\Controller\Abstracts;

use Sto\Services\Core\Controller\Contracts\WebControllerInterface;

/**
 * Class WebController.
 *
 * 
 */
abstract class WebController extends MasterController implements WebControllerInterface
{

}
