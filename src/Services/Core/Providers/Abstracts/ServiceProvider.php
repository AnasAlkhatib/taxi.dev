<?php

namespace Sto\Services\Core\Providers\Abstracts;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Sto\Services\Core\Providers\Traits\MasterServiceProviderTrait;

/**
 * Class ServiceProvider.
 *
 * 
 */
abstract class ServiceProvider extends LaravelServiceProvider
{

    use MasterServiceProviderTrait;
}
