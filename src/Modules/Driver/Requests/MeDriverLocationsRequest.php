<?php

namespace Sto\Modules\Driver\Requests;


use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class MeDriverLocationsRequest
 * @package Sto\Modules\Driver\Requests
 */
class MeDriverLocationsRequest extends Request
{
        /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude'  => ['filled','regex:/^([-+]?\d{1,2}([.]\d+)?)$/'],
            'longitude' => ['filled','regex:/\s*([-+]?\d{1,3}([.]\d+)?)$/']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}