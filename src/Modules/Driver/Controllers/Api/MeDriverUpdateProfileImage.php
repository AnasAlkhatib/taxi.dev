<?php

namespace Sto\Modules\Driver\Controllers\Api;


use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Requests\MeDriverUpdateProfileImageRequest;
use Sto\Modules\Driver\Tasks\MeDriverUpdateProfileImageTask;
use Sto\Modules\Driver\Transformers\DriverTransformer;

class MeDriverUpdateProfileImage extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/drivers/me/settings/picture",
     *      summary="Change picture",
     *      tags={"Driver Account"},
     *      consumes={"multipart/form-data"},
     *      @SWG\Parameter(
     *         description="file to upload",
     *         in="formData",
     *         name="picture_url",
     *         required=false,
     *         type="file"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverUpdateResponse"
     *              )
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */
    public function handle
    (
        MeDriverUpdateProfileImageRequest $meDriverUpdateProfileImageRequest,
        MeDriverUpdateProfileImageTask $meDriverUpdateProfileImageTask
    )
    {
        $result = $meDriverUpdateProfileImageTask->run($meDriverUpdateProfileImageRequest);
        return $this->response->item($result, new DriverTransformer());
    }

}