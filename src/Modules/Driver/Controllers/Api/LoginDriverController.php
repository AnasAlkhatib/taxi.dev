<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Modules\Driver\Requests\LoginDriverRequest;
use Sto\Modules\Driver\Tasks\LoginDriverTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class LoginController.
 *
 * 
 */
class LoginDriverController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/drivers/login",
     *      summary="Generate auth token for driver",
     *      tags={"Authentication Driver"},
     *      description="Generate authentication token based on email and password",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          default="{""email"":""driver@maued.dev"",""password"":""1234567""}",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  description="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverSuccessResponse"
     *              ),
     *          )
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Credentials Incorrect.",
     *     ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */
    /**
     * @param LoginDriverRequest $loginDriverRequest
     * @param LoginDriverTask $loginTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(LoginDriverRequest $loginDriverRequest, LoginDriverTask $loginDriverTask)
    {
        $user = $loginDriverTask->run($loginDriverRequest['email'], $loginDriverRequest['password']);
        return $this->response->item($user, new UserTransformer());
    }
}
