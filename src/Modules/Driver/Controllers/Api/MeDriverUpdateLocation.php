<?php

namespace Sto\Modules\Driver\Controllers\Api;

use Sto\Modules\Driver\Transformers\DriverLocationTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Driver\Requests\MeDriverLocationsRequest;
use Sto\Modules\Driver\Tasks\MeDriverLocationsTask;

/**
 * Class MeDriverUpdateLocation
 * @package Sto\Modules\Driver\Controllers\Api
 */
class MeDriverUpdateLocation extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/drivers/me/settings/locations",
     *      summary="Update Driver Location",
     *      tags={"Driver Account"},
     *      description="Update driver location latitude and longitude",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
         *                  property="latitude",
     *                  description="latitude",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="longitude",
     *                  description="longitude Password",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DriverLocationSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */

    /**
     * @param MeDriverLocationsRequest $locationsRequest
     * @param MeDriverLocationsTask $locationsTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        MeDriverLocationsRequest $locationsRequest,
        MeDriverLocationsTask $locationsTask
    ) {
        $result = $locationsTask->run($locationsRequest);
        return $this->response->item($result, new DriverLocationTransformer());
    }
}