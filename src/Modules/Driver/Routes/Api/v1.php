<?php
/**
 * Driver Module Routes File.
 */

$router->post('login', [
    'uses' => 'LoginDriverController@handle',
]);
$router->post('register', [
    'uses' => 'RegisterDriverController@handle',
]);

//$router->get('test', function(){
//    return Sto\Modules\Driver\Models\Car::find(1)->type;
//});

$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:driver'
            ]
    ], function ($router) {

    $router->post('me/logout', ['uses' => 'LogoutDriverController@handle']);
    $router->get('me', ['uses' => 'MeDriverDetailsController@handle']);
    $router->put('me/settings/password', ['uses' => 'MeDriverChangePasswordController@handle']);
    $router->put('me/settings/basic', ['uses' => 'MeDriverUpdateBasicInfoController@handle']);
    $router->put('me/settings/locations', ['uses' => 'MeDriverUpdateLocation@handle']);
    $router->put('me/settings/cars', ['uses' => 'MeDriverUpdateCarInfo@handle']);
    $router->put('me/settings/availablilty', ['uses' => 'MeDriverUpdateAvailablilty@handle']);
    $router->post('me/settings/picture', ['uses' => 'MeDriverUpdateProfileImage@handle']);

});




