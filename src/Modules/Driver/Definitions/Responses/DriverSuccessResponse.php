<?php
namespace Sto\Modules\Driver\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="DriverSuccessResponse",
 *      required={"name", "email", "password", "phone_number"},
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="token",
 *          description="token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="role",
 *          description="role",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture_url",
 *          description="picture_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="available",
 *          description="available",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="object"
 *      ),
 * )
 */


class DriverSuccessResponse
{

}