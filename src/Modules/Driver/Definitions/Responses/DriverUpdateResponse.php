<?php

namespace Sto\Modules\Driver\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="DriverUpdateResponse",
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="locale",
 *          description="locale",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture_url",
 *          description="picture_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="available",
 *          description="available",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="car",
 *          description="car",
 *          type="object",
 *          @SWG\Property(
 *              property="car_type",
 *              description="car_type",
 *              type="string"
 *          ),
 *          @SWG\Property(
 *              property="car_number",
 *              description="car_number",
 *              type="string"
 *          ),
 *          @SWG\Property(
 *              property="car_model",
 *              description="car_model",
 *              type="string"
 *          ),
 *          @SWG\Property(
 *              property="car_year",
 *              description="car_year",
 *              type="string"
 *          ),
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="object"
 *      ),
 * )
 */

class DriverUpdateResponse
{

}