<?php

namespace Sto\Modules\Driver\Models;


use Sto\Modules\Ride\Models\CarType;
use Sto\Modules\User\Models\User;
use Sto\Services\Core\Model\Abstracts\Model;

class Car extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'car_type_id',
        'car_number',
        'car_model',
        'car_year',
    ];

    /**
     * The dates attributes.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(CarType::Class,'car_type_id', 'id');
    }


}