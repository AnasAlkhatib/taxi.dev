<?php

namespace Sto\Modules\Driver\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CarRepositoryInterface
 * @package Sto\Modules\Driver\Contracts
 */
interface CarRepositoryInterface extends RepositoryInterface
{

}
