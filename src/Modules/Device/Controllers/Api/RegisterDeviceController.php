<?php

namespace Sto\Modules\Device\Controllers\Api;

use Sto\Modules\Device\Requests\RegisterRequest;
use Sto\Modules\Device\Tasks\CreateDeviceTask;
use Sto\Modules\Device\Transformers\DeviceTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class RegisterDeviceController.
 *
 * 
 */
class RegisterDeviceController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/devices",
     *      summary="Insert user device information.",
     *      tags={"Devices"},
     *      description="Insert user device information.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="device that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/Device"
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeviceSuccessPostedResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */
    /**
     * @param RegisterRequest $registerRequest
     * @param CreateDeviceTask $createDeviceTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        RegisterRequest $registerRequest,
        CreateDeviceTask $createDeviceTask
    ) {

        $device = $createDeviceTask->run($registerRequest);

        return $this->response->item($device, new DeviceTransformer());
    }
}
