<?php

namespace Sto\Modules\Device\Tasks;

use Sto\Modules\Device\Contracts\DeviceRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Device\Requests\RegisterRequest;

/**
 * Class CreateDeviceTask
 * @package Sto\Modules\Device\Tasks
 */
class CreateDeviceTask extends Task
{

    /**
     * @var \Sto\Modules\Device\Contracts\DeviceRepositoryInterface
     */
    private $deviceRepository;

    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * CreateDeviceTask constructor.
     * @param DeviceRepositoryInterface $deviceRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(DeviceRepositoryInterface $deviceRepository, AuthenticationService $authenticationService)
    {
        $this->deviceRepository = $deviceRepository;
        $this->authenticationService = $authenticationService;
    }

    public function run(RegisterRequest $registerRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $registerRequest->all();
        $data['user_id'] = $user->id;

        return $this->deviceRepository->create($data);
    }
}
