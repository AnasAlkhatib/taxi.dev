<?php
/**
 * Device Module Routes File.
 */

$router->get('/ping', function () {
    return 'Device Module.';
});

$router->group(
    [
        'middleware' =>
            [
                'api.auth'
            ]
    ], function ($router) {

   $router->post('/', ['uses' => 'RegisterDeviceController@handle',]);
});





