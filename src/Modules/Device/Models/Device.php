<?php

namespace Sto\Modules\Device\Models;

use Sto\Services\Core\Model\Abstracts\Model;

/**
 * Class Device.
 *
 * 
 */
class Device extends Model

{

    // use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'registraion_id',
        'os',
        'os_version',
        'app_version',
        'app_type',
        'device_model',
        'location_lat',
        'location_lon'
    ];

    /**
     * The dates attributes.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
}
