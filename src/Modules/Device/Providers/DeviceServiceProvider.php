<?php

namespace Sto\Modules\Device\Providers;

use Sto\Modules\Device\Contracts\DeviceRepositoryInterface;
use Sto\Modules\Device\Repositories\Eloquent\DeviceRepository;
use Sto\Services\Core\Providers\Abstracts\ServiceProvider;

/**
 * Class DeviceServiceProvider.
 *
 * 
 */
class DeviceServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Module Service providers to be registered.
     *
     * @var array
     */
    protected $providers = [
        RoutesServiceProvider::class,
        AuthServiceProvider::class,
    ];

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        $this->registerServiceProviders($this->providers);
    }

    /**
     * Register bindings in the container.
     */
    public function register()
    {
        $this->registerTheDatabaseMigrationsFiles(__DIR__);

        $this->app->bind(DeviceRepositoryInterface::class, DeviceRepository::class);
    }
}
