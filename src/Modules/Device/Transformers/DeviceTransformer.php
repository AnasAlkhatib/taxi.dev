<?php

namespace Sto\Modules\Device\Transformers;

use Sto\Modules\Device\Models\Device;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class DeviceTransformer.
 *
 *
 */
class DeviceTransformer extends Transformer
{

    /**
     * @param \Sto\Modules\Device\Models\Device $user
     *
     * @return array
     */
    public function transform(Device $device)
    {
        return [
            'user_id' => $device->user_id,
            'registraion_id' => $device->registraion_id,
            'os' => $device->os,
            'os_version' => $device->os_version,
            'app_version' => $device->app_version,
            'app_type' => $device->app_type,
            'device_model' => $device->device_model,
            'location_lat' => $device->location_lat,
            'location_lon' => $device->location_lon,
        ];
    }
}
