<?php

namespace Sto\Modules\Device\Repositories\Eloquent;

use Sto\Modules\Device\Contracts\DeviceRepositoryInterface;
use Sto\Modules\Device\Models\Device;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class DeviceRepository.
 *
 * 
 */
class DeviceRepository extends Repository implements DeviceRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'  => 'like',
        'email' => '=',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Device::class;
    }
}
