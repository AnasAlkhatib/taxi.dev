<?php


namespace Sto\Modules\Ride\Requests\Rider;

use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class GetFareConditionsRequest
 * @package Sto\Modules\Ride\Requests\Rider
 */


class GetFareConditionsRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function Rules()
    {
        return [];
        
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    
}