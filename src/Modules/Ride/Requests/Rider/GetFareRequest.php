<?php


namespace Sto\Modules\Ride\Requests\Rider;


use Sto\Services\Core\Request\Abstracts\Request;

/**
 * Class GetFareRequest
 * @package Sto\Modules\Ride\Requests\Rider
 */
class GetFareRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude_from' => ['filled', 'regex:/^([-+]?\d{1,2}([.]\d+)?)$/'],
            'longitude_from' => ['filled', 'regex:/\s*([-+]?\d{1,3}([.]\d+)?)$/'],
            'latitude_to' => ['filled', 'regex:/^([-+]?\d{1,2}([.]\d+)?)$/'],
            'longitude_to' => ['filled', 'regex:/\s*([-+]?\d{1,3}([.]\d+)?)$/'],
            'car_type_id' => ['filled'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}