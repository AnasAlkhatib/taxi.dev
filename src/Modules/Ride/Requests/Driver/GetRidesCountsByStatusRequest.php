<?php


namespace Sto\Modules\Ride\Requests\Driver;


use Sto\Services\Core\Request\Abstracts\Request;

class GetRidesCountsByStatusRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' =>  'required|integer|in:2,3,4,5,6,7',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}