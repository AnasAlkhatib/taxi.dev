<?php


namespace Sto\Modules\Ride\Controllers\Api\Rider;


use Sto\Modules\Ride\Transformers\CarTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\GetCarsRequest;
use Sto\Modules\Ride\Tasks\Rider\GetCarsTask;

/**
 * Class GetCarsController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class GetCarsController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/cars",
     *      summary="Get all cars.",
     *      tags={"Rider Rides"},
     *      description="Get All Cars.",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CarsSuccessResponse"
     *              ),
     *          )
     *      )
     * )
     */
    /**
     * @param GetCarsRequest $getCarsRequest
     * @param GetCarsTask $getCarsTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle
    (
        GetCarsRequest $getCarsRequest,
        GetCarsTask $getCarsTask
    )
    {
        $cars = $getCarsTask->run($getCarsRequest);
        return $this->response()->paginator($cars, new CarTransformer());
    }
}
