<?php

namespace Sto\Modules\Ride\Controllers\Api\Rider;

use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\PutRideStatusRequest;
use Sto\Modules\Ride\Tasks\Rider\PutRideStatusTask;
use Sto\Modules\Ride\Transformers\RideTransformer;

class PutRideStatusController extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/rides/me/statuses",
     *      summary="Update ride status.",
     *      tags={"Rider Rides"},
     *      description="Update ride status, statuses are
    [
    1 => STARTED ,
    2 => PROCESSING ,
    3 => CANCELED ,
    4 => ACCEPTED ,
    5 => REJECTED ,
    6 => IN_PROGRESS ,
    7 => COMPLETED
    ]",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="uuid",
     *                  description="UUID Key for ride.",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ride_status_id",
     *                  description="ride_statuses_id 1, 2, 3, 6, 7",
     *                  type="integer"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     )
     * )
     */
    /**
     * @param PutRideStatusRequest $putRideStatusRequest
     * @param PutRideStatusTask $putRideStatusTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        PutRideStatusRequest $putRideStatusRequest,
        PutRideStatusTask $putRideStatusTask
    ) {
        $result = $putRideStatusTask->run($putRideStatusRequest);
        return $this->response->item($result, new RideTransformer());
    }
}