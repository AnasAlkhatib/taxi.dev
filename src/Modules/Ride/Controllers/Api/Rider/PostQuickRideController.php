<?php

namespace Sto\Modules\Ride\Controllers\Api\Rider;


use Sto\Modules\Ride\Exceptions\RideFailedException;
use Sto\Modules\Ride\Transformers\QuickRideCreatedTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\PostQuickRideRequest;
use Sto\Modules\Ride\Tasks\Rider\PostQuickRideTask;
use Log;

/**
 * Class PostQuickRideController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class PostQuickRideController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/rides/me/quick",
     *      summary="Request a ride",
     *      tags={"Rider Rides"},
     *      description="Issue a quick ride request.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ride that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              ref="#/definitions/QuickRide"
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideSuccessPostedResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */

    /**
     * @param PostQuickRideRequest $postRideRequest
     * @param PostQuickRideTask $postRideTask
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(
        PostQuickRideRequest $postRideRequest,
        PostQuickRideTask $postRideTask
    )
    {
        try {
            $result = $postRideTask->run($postRideRequest);
            return $this->response->item($result, new QuickRideCreatedTransformer());
        } catch (RideFailedException $e) {
            Log::error($e->getMessage());
        }
    }
}