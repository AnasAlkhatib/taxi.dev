<?php


namespace Sto\Modules\Ride\Controllers\Api\Rider;


use Sto\Modules\Ride\Transformers\CarTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\GetFareRequest;
use Sto\Modules\Ride\Tasks\Rider\GetFareTask;

/**
 * Class FareController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class FareController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/rides/fare",
     *      summary="Request a fare for a ride ",
     *      tags={"Rider Rides"},
     *      description="Issue a ride request.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ride that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="latitude_from",
     *                  description="Latitude From",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="longitude_from",
     *                  description="Longitude From",
     *                  type="string"
     *              ),
     *               @SWG\Property(
     *                  property="latitude_to",
     *                  description="Latitude To",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="longitude_to",
     *                  description="Longitude To",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="car_type_id",
     *                  description="car_type_id <br> 1 => Taxi <br> 2 => Van",
     *                  type="integer"
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideSuccessPostedResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */

    /**
     * @param GetFareRequest $getFareRequest
     * @param GetFareTask $getFareTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle
    (
        GetFareRequest $getFareRequest,
        GetFareTask $getFareTask
    )
    {
        $fare = $getFareTask->run($getFareRequest);
        
        return response()->json(['data' => 
            ["fare" => $fare]
        ]);
    }
}
