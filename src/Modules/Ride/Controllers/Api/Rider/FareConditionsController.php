<?php


namespace Sto\Modules\Ride\Controllers\Api\Rider;

use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Rider\GetFareConditionsRequest;
use Sto\Modules\Ride\Tasks\Rider\GetFareConditionsTask;


/**
 * Class FareConditionsController
 * @package Sto\Modules\Ride\Controllers\Api\Rider
 */
class FareConditionsController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/farecond",
     *      summary="Get Fare Calculation General Rules for ride ",
     *      tags={"Rider Rides"},
     *      description="Get Fare Calculation General Rules for ride.",
     *      produces={"application/json"},     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",     *
     *      ),
     *
     * )
     */
    public function handle
    (
        GetFareConditionsRequest $getFareConditionsRequest,
        GetFareConditionsTask $getFareConditionsTask
    )
    {
        $fareconditions = $getFareConditionsTask->run($getFareConditionsRequest);

        return response()->json(['data' =>
            ["fare" => $fareconditions]
        ]);






    }
}
