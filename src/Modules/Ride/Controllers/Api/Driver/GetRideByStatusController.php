<?php

namespace Sto\Modules\Ride\Controllers\Api\Driver;

use Sto\Modules\Ride\Exceptions\RideRetrieveException;
use Sto\Modules\Ride\Transformers\RideDetailsTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Driver\GetRideByStatusRequest;
use Sto\Modules\Ride\Tasks\Driver\GetRideByStatusTask;
use Log;

class GetRideByStatusController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/r/statuses/",
     *      summary="Get rides by status for driver.",
     *      tags={"Driver Rides"},
     *      description="Get My Ride Requests. <br > http://url.dev/api/v1/rides/r/statuses/?status={status_id}",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *         description="status id",
     *         name="status",
     *         in="query",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideDetailsResponse"
     *              ),
     *          )
     *      )
     * )
     */

    /**
     * @param GetRideByStatusRequest $getRideByStatusRequest
     * @param GetRideByStatusTask $getRideByStatusTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        GetRideByStatusRequest $getRideByStatusRequest,
        GetRideByStatusTask $getRideByStatusTask
    )
    {

        try {
            $result = $getRideByStatusTask->run($getRideByStatusRequest);
            return $this->response->paginator($result, new RideDetailsTransformer());
        } catch (RideRetrieveException $e) {
            Log::error($e->getMessage());
        }
    }
}