<?php

namespace Sto\Modules\Ride\Controllers\Api\Driver;

use Sto\Modules\Ride\Exceptions\RideRetrieveException;
use Sto\Modules\Ride\Transformers\RidesCountsTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Driver\GetRidesCountsByStatusRequest;
use Sto\Modules\Ride\Tasks\Driver\GetRidesCountsByStatusTask;
use Log;

class GetRidesCountsByStatusController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/me/counts/statuses/",
     *      summary="Get rides counts by status for driver",
     *      tags={"Driver Rides"},
     *      description="<strong>Get My Ride Counts.</strong> <br> using /rides/me/counts/statuses/?status={ride_status_id}.",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *         description="status id",
     *         name="status",
     *         in="query",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *      )
     * )
     */

    /**
     * @param GetRidesCountsByStatusRequest $getRidesCountsByStatusRequest
     * @param GetRidesCountsByStatusTask $getRidesCountsByStatusTask
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(
        GetRidesCountsByStatusRequest $getRidesCountsByStatusRequest,
        GetRidesCountsByStatusTask $getRidesCountsByStatusTask
    )
    {

        try {
            $result = $getRidesCountsByStatusTask->run($getRidesCountsByStatusRequest);
            return response()->json(['count' => $result]);
        } catch (RideRetrieveException $e) {
            Log::error($e->getMessage());
        }
    }
}