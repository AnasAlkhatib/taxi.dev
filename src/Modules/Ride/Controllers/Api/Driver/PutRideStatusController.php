<?php

namespace Sto\Modules\Ride\Controllers\Api\Driver;

use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Driver\PutRideStatusRequest;
use Sto\Modules\Ride\Tasks\Driver\PutRideStatusTask;
use Sto\Modules\Ride\Transformers\DriverRideTransformer;

class PutRideStatusController extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/rides/drivers/statuses",
     *      summary="Update ride status.",
     *      tags={"Driver Rides"},
     *      description="Update ride status, statuses are
    [
    4 => ACCEPTED ,
    6 => IN_PROGRESS ,
    7 => COMPLETED
    ]",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="uuid",
     *                  description="UUID Key for ride.",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="ride_status_id",
     *                  description="ride_statuses_id 4,6,7",
     *                  type="integer"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     )
     * )
     */
    /**
     * @param PutRideStatusRequest $putRideStatusRequest
     * @param PutRideStatusTask $putRideStatusTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        PutRideStatusRequest $putRideStatusRequest,
        PutRideStatusTask $putRideStatusTask
    ) {
        $result = $putRideStatusTask->run($putRideStatusRequest);
        if(is_null($result)){
            return $this->response->accepted();
        }

        return $this->response->item($result, new DriverRideTransformer());

    }
}