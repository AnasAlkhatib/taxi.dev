<?php

namespace Sto\Modules\Ride\Controllers\Api\Driver;

use Sto\Modules\Ride\Exceptions\RideRetrieveException;
use Sto\Modules\Ride\Transformers\ActiveRideTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Driver\GetActiveRideRequest;
use Sto\Modules\Ride\Tasks\Driver\GetActiveRideTask;
use Log;

class GetActiveRideController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/r/bing",
     *      summary="Get current running or active ride.",
     *      tags={"Driver Rides"},
     *      description="",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RideDetailsResponse"
     *              ),
     *          )
     *      )
     * )
     */

    /**
     * @param GetActiveRideRequest $getActiveRideRequest
     * @param GetActiveRideTask $getActiveRideTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        GetActiveRideRequest $getActiveRideRequest,
        GetActiveRideTask $getActiveRideTask
    )
    {

        try {
            $result = $getActiveRideTask->run($getActiveRideRequest);
            return $this->response->item($result, new ActiveRideTransformer());
        } catch (RideRetrieveException $e) {
            Log::error($e->getMessage());
        }
    }
}