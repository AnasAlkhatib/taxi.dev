<?php

namespace Sto\Modules\Ride\Controllers\Api\Admin;

use Sto\Modules\Ride\Exceptions\RideRetrieveException;
use Sto\Modules\Ride\Transformers\RideDetailsTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Ride\Requests\Admin\GetRidesRequest;
use Sto\Modules\Ride\Tasks\Admin\GetRidesTask;
use Log;

class GetRidesController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/rides/admins/",
     *      summary="Get rides for admin",
     *      tags={"Admin Rides"},
     *      description="<strong>Get My Rides.</strong>",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *      )
     * )
     */

    /**
     * @param GetRidesRequest $getRidesCountsByStatusRequest
     * @param GetRidesTask $getRidesCountsByStatusTask
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(
        GetRidesRequest $getRidesCountsByStatusRequest,
        GetRidesTask $getRidesCountsByStatusTask
    )
    {

        try {
            $result = $getRidesCountsByStatusTask->run($getRidesCountsByStatusRequest);
            return $this->response->paginator($result, new RideDetailsTransformer());
        } catch (RideRetrieveException $e) {
            Log::error($e->getMessage());
        }
    }
}