<?php

namespace Sto\Modules\Ride\Exceptions;

use Sto\Services\Core\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

class RideRetrieveException extends ApiException
{

    public $httpStatusCode = Response::HTTP_FAILED_DEPENDENCY;
    public $message = 'Failed getting your rides.';
}