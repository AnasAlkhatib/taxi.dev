<?php


namespace Sto\Modules\Ride\Transformers;


use Sto\Services\Core\Transformer\Abstracts\Transformer;
use Sto\Modules\Ride\Models\Ride;

class RideDetailsTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(Ride $ride)
    {
        return [
            'uuid' => $ride->uuid,
            'status' => $ride->rideStatus->status,
            'rider' => $ride->user->name,
            'driver' => ($ride->accepted) ? [
                'driver_id' => $ride->accepted->user->id,
                'name' => $ride->accepted->user->name,
                'car' => [
                    'type' => ($ride->accepted->user->cars->first()) ? $ride->accepted->user->cars->first()->car_type_id : null,
                    'number' => ($ride->accepted->user->cars->first()) ? $ride->accepted->user->cars->first()->car_number : null,
                    'model' => ($ride->accepted->user->cars->first()) ? $ride->accepted->user->cars->first()->car_model : null,
                    'year' => ($ride->accepted->user->cars->first()) ? $ride->accepted->user->cars->first()->car_year : null,
                ],
            ] : null,
            'fare' => $ride->fare,
            'eta' => ($ride->latitude_to && $ride->longitude_to) ?
                \Sto\Services\GeoTools\GeoTools::getEta(
                    $ride->latitude_from,
                    $ride->longitude_from,
                    $ride->latitude_to,
                    $ride->longitude_to
                ) : null,
            'from' => [
                'latitude_from' => $ride->latitude_from,
                'longitude_from' => $ride->longitude_from,
                'from_txt' => $ride->from_txt,
            ],
            'to' => [
                'latitude_to' => $ride->latitude_to,
                'longitude_to' => $ride->longitude_to,
                'to_txt' => $ride->to_txt,
            ],
            'people' => $ride->people,
            'updated_at' => $ride->updated_at,
            'created_at' => $ride->created_at,
        ];
    }

}