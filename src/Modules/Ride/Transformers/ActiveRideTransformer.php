<?php

namespace Sto\Modules\Ride\Transformers;

use Sto\Modules\Ride\Models\Ride;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class ActiveRideTransformer
 * @package Sto\Modules\Ride\Transformers
 */
class ActiveRideTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(Ride $ride)
    {
        return [
            'uuid'       => $ride->uuid,
            'status'     => $ride->rideStatus->status,
            'rider'      => $ride->user->name,
            'fare'       => ($ride->fare)? $ride->fare : 0.0,
            'eta'        => ($ride->latitude_to && $ride->longitude_to) ?
                \Sto\Services\GeoTools\GeoTools::getEta(
                    $ride->latitude_from,
                    $ride->longitude_from,
                    $ride->latitude_to,
                    $ride->longitude_to
                ) : null,
            'from'       =>[
                'latitude_from' => $ride->latitude_from,
                'longitude_from' => $ride->longitude_from,
                'from_txt' => $ride->from_txt,
            ],
            'to'       =>[
                'latitude_to' => $ride->latitude_to,
                'longitude_to' => $ride->longitude_to,
                'to_txt' => $ride->to_txt,
            ],
            'people' => $ride->people,
            'created_at' => $ride->created_at,
            'updated_at' => $ride->updated_at,
        ];
    }

}
