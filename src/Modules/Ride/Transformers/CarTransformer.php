<?php

namespace Sto\Modules\Ride\Transformers;

use Sto\Modules\Ride\Models\Ride;
use Sto\Modules\Ride\Models\CarType;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class CarTransformer
 * @package Sto\Modules\Ride\Transformers
 */
class CarTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(CarType $car)
    {
        return [
            'car_id' => $car->id,
            'type' => $car->type
        ];
    }
    
}
