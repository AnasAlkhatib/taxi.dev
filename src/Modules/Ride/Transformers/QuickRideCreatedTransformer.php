<?php

namespace Sto\Modules\Ride\Transformers;

use Sto\Modules\Ride\Models\Ride;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class QuickRideCreatedTransformer
 * @package Sto\Modules\Ride\Transformers
 */
class QuickRideCreatedTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(Ride $ride)
    {
        return [
            'uuid'       => $ride->uuid,
            'fare'       => $ride->fare,
            'eta'        => null,
            'created_at' => $ride->created_at,
            'updated_at' => $ride->updated_at,
        ];
    }

}
