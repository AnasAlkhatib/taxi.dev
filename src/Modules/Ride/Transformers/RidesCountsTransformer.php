<?php


namespace Sto\Modules\Ride\Transformers;

use Sto\Modules\Ride\Models\Ride;
use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

/**
 * Class RidesCountsTransformer
 * @package Sto\Modules\Ride\Transformers
 */
class RidesCountsTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(Ride $ride)
    {
        return [
            'status' => $ride->rideStatus->status,
            'count' => $ride->count
        ];
    }
}