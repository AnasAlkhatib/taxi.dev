<?php


namespace Sto\Modules\Ride\Transformers;

use Sto\Modules\Ride\Models\DriverRide;
use Sto\Modules\Ride\Models\Ride;
use Sto\Modules\User\Models\User;
use Sto\Services\Core\Transformer\Abstracts\Transformer;

class DriverRideTransformer extends Transformer
{
    /**
     * @param Ride $ride
     * @return array
     */
    public function transform(DriverRide $ride)
    {
        return [
            'uuid'       => $ride->uuid,
            'Rider'      => Ride::find($ride->uuid)->user->name,
            'Driver'     => $ride->user->name,
            'status'     => $ride->rideStatus->status,
            'created_at' => $ride->created_at,
            'updated_at' => $ride->updated_at,
        ];
    }

}