<?php
/**
 * Ride Module Routes File.
 */




$router->get('/ping', function () {
    return response()->json(['status' => 'Online.']);
});

$router->get('test', function(){
    //$users = Sto\Modules\User\Models\User::all();
//    Sto\Services\PushService\PushService::pushSingleMessage(
//        'ae93fb97dd7a6b192094da3c0fbac6bf89c4faeace4f7be8d0a933840d2c197b',
//        'message driver',
//        'DriverAppIOS'
//    );


/*    Sto\Services\PushService\PushService::pushSingleMessage(
        '0e17f4d8c30133d38ba6a4737383d0796be13d5d59e1795f05c45a40cf7a0878',
           Sto\Services\PushService\PushService::setAcceptCustomMessage(1,"02645b10-4e5d-11e6-86e0-cbd57c4f5578",'now','driver name'
                                                                         ,'0786017821','ff96c990-4e54-11e6-be4d-ff28355487cd'),
        'DriverAppIOS'
    );*/

 /*Sto\Services\PushService\PushService::pushSingleMessage(
        'APA91bE8ZcZHxrtNLckMsAty-_xbeBk06OWSSvz6sXbPmpngvlL4Yyp68c2bBBmra3LLU3oDn_WxxPEjMuUWG4YoaRepdCTaCnhSAlwrdQ2QYCYjCGIWSdAK8vFmsTgdg9umL18BW983',
     Sto\Services\PushService\PushService::setAcceptCustomMessage(1,"02645b10-4e5d-11e6-86e0-cbd57c4f5578",'now','driver name'
         ,'0786017821','ff96c990-4e54-11e6-be4d-ff28355487cd'),
        'DriverAndroid'
    );
*/


    //Sto\Services\PushService\PushService::pushMultipleMessage($users,'message');

});
$router->get('test2', function(){
    return \Sto\Modules\Ride\Models\DriverRide::find('9')->user->name;
});
$router->get('test3', function(){
    $v = Sto\Services\GeoTools\GeoTools::getDistance('32.05371890', '35.89969350', '32.037704', '35.864493');
    $fare = \Sto\Services\Fare\Fare::fareCalculator($v, 0.25);
    return \Sto\Services\GeoTools\GeoTools::getEta('32.05371890', '35.89969350', '32.037704', '35.864493');
    //echo "distance : ", $v, PHP_EOL ;
    //echo "time : ", $time, PHP_EOL ;
});
/**
 * Rider Ride Routes
 */
$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:rider'
            ]
    ], function ($router) {

    $router->post('me', ['uses' => 'Rider\PostRideController@handle']);
    $router->post('/me/quick', ['uses' => 'Rider\PostQuickRideController@handle']);
    $router->get('me', ['uses' => 'Rider\GetRidesController@handle']);
    $router->put('me/statuses', ['uses' => 'Rider\PutRideStatusController@handle']);
    $router->get('/', ['uses' => 'Rider\GetRideController@handle']);
    $router->get('me/statuses/', ['uses' => 'Rider\GetRideByStatusController@handle']);
    $router->get('/me/nearby', ['uses' => 'Rider\GetNearByRideController@handle']);
    $router->get('/cars', ['uses' => 'Rider\GetCarsController@handle']);
    $router->post('/fare', ['uses' => 'Rider\FareController@handle']);
    $router->get('/farecond', ['uses' => 'Rider\FareConditionsController@handle']);


    
});

/**
 * Driver Ride Routes
 */
$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:driver'
            ]
    ], function ($router) {

    $router->get('/me/counts/statuses', ['uses' => 'Driver\GetRidesCountsByStatusController@handle']);
    $router->put('/drivers/statuses', ['uses' => 'Driver\PutRideStatusController@handle']);
    $router->get('/r/', ['uses' => 'Driver\GetRideController@handle']);
    $router->get('/r/statuses', ['uses' => 'Driver\GetRideByStatusController@handle']);
    $router->get('/r/bing', ['uses' => 'Driver\GetActiveRideController@handle']);
});


/**
 * Driver Ride Routes
 */
$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:admin'
            ]
    ], function ($router) {

    $router->get('/admins', ['uses' => 'Admin\GetRidesController@handle']);
});


