<?php
namespace Sto\Modules\Ride\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="RideSuccessPostedResponse",
 *      @SWG\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="object"
 *      ),
 * )
 */

class RideSuccessPostedResponse
{
}