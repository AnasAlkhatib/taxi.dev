<?php

namespace Sto\Modules\Ride\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="RideNearBySuccessResponse",
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_is",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="distance",
 *          description="distance",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="role",
 *          description="role",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="number",
 *          format="float"
 *      ),
 *
 * )
 */

class RideNearBySuccessResponse
{

}