<?php

namespace Sto\Modules\Ride\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="RideDetailsResponse",
 *      @SWG\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rider",
 *          description="rider",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="driver",
 *          description="driver",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="from",
 *          description="from",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="to",
 *          description="to",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="people",
 *          description="people",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="object"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="object"
 *      ),
 * )
 */

class RideDetailsResponse
{

}