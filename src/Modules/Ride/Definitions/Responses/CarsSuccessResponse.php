<?php


namespace Sto\Modules\Ride\Definitions\Responses;

/**
 * @SWG\Definition(
 *      definition="CarsSuccessResponse",
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      )
 * )
 */

class CarsSuccessResponse
{

}