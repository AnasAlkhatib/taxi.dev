<?php


namespace Sto\Modules\Ride\Tasks\Rider;

use Sto\Modules\Ride\Requests\Rider\GetFareRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Services\Fare\Fare;
use Sto\Services\GeoTools\GeoTools;
use Sto\Modules\Ride\Repositories\Eloquent\CarTypeRepository;

/**
 * Class GetFareTask
 * @package Sto\Modules\Ride\Tasks\Rider
 */
class GetFareTask extends Task
{
    private  $carTypeRepository;
    public function __construct(CarTypeRepository $carTypeRepository)
    {
        $this->carTypeRepository = $carTypeRepository;
    }

    /**
     * @param GetFareRequest $getFareRequest
     * @return mixed
     */
    public function run(GetFareRequest $getFareRequest)
    {

        $car = $this->carTypeRepository->find($getFareRequest->car_type_id);
        $basePrice = floatval(env('FARE_BASE_PRICE')) + $car->base_price;
        return ceil(Fare::fareCalculator(
            GeoTools::getDistance(
                $getFareRequest->latitude_from,
                $getFareRequest->longitude_from,
                $getFareRequest->latitude_to,
                $getFareRequest->longitude_to
            ), $basePrice));
        
    }
}