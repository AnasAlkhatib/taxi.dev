<?php

namespace Sto\Modules\Ride\Tasks\Rider;


use Sto\Modules\Ride\Repositories\Criterias\Eloquent\ThisUserCriteria;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;

class GetRidesTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var \Sto\Modules\Ride\Contracts\RideRepositoryInterface
     */
    private $rideRepository;

    /**
     * PostUserRideTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
        $this->rideRepository = $rideRepository;
    }


    public function run()
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $this->rideRepository->pushCriteria(new ThisUserCriteria($user->id));
        $this->rideRepository->pushCriteria(new OrderByCreationDateDescendingCriteria());

        $rides = $this->rideRepository->paginate();
        return $rides;
    }

}