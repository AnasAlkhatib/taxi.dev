<?php

namespace Sto\Modules\Ride\Tasks\Rider;


use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\Ride\Requests\Rider\PutRideStatusRequest;
use Sto\Modules\User\Repositories\Criterias\Eloquent\DriverTokenByRideIdCriteria;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\PushService\PushService;

class PutRideStatusTask extends Task
{
    private $authenticationService;
    private $rideRepository;
    private $driverRideRepository;

    /**
     * PutRideStatusTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param AuthenticationService $authenticationService
     * @param DriverRideRepositoryInterface $driverRideRepository
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        AuthenticationService $authenticationService,
        DriverRideRepositoryInterface $driverRideRepository,
        UserRepositoryInterface $userRepository
    ) {
        $this->authenticationService = $authenticationService;
        $this->rideRepository = $rideRepository;
        $this->driverRideRepository = $driverRideRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param PutRideStatusRequest $putRideStatusRequest
     * @return mixed
     */
    public function run(PutRideStatusRequest $putRideStatusRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $found  = $this->driverRideRepository->findByField('uuid', $putRideStatusRequest['uuid']);


        if ((boolean)$found === true){

            $this->driverRideRepository->update(['ride_status_id' => $putRideStatusRequest['ride_status_id']], $found->first()->id);
             if($putRideStatusRequest['ride_status_id'] == 3)
             {
                 $driver=$this->userRepository->pushCriteria(new DriverTokenByRideIdCriteria($putRideStatusRequest['uuid']))->first();

                //send to rider
                 if($user['os']=='Android' && !empty($user['device_token']))

                     PushService::pushSingleMessage($user['device_token'], PushService::setCancelMessage($putRideStatusRequest['uuid'],'Ride is cancelled ',4),'RiderAndroid');
                 else if ($user['os']=='IOS' && !empty($user['device_token']))
                     PushService::pushSingleMessage($user['device_token'], PushService::setCancelMessage($putRideStatusRequest['uuid'],'Ride is cancelled ',4),'RiderAppIOS');

                 //send to driver
                 if($driver['os']=='Android'  && !empty($driver['device_token']))
                     PushService::pushSingleMessage($driver['device_token'],PushService::setCancelMessage($putRideStatusRequest['uuid'],'Ride is cancelled ',4) ,'DriverAndroid');
                 else if ($driver['os']=='IOS' && !empty($driver['device_token']))
                     PushService::pushSingleMessage($driver['device_token'],PushService::setCancelMessage($putRideStatusRequest['uuid'],'Ride is cancelled ',4),'DriverAppIOS');

             }


        }

        return $this
            ->rideRepository
            ->update(
                $putRideStatusRequest->all(),
                $putRideStatusRequest['uuid']
            );

    }

}