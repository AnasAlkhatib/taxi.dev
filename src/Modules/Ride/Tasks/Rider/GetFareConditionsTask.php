<?php


namespace Sto\Modules\Ride\Tasks\Rider;

use Sto\Modules\Ride\Requests\Rider\GetFareConditionsRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Repositories\Eloquent\CarTypeRepository;

/**
 * Class GetFareConditionsTask
 * @package Sto\Modules\Ride\Tasks\Rider
 */
class GetFareConditionsTask extends Task
{
    private $carTypeRepository;

    public function __construct(
        CarTypeRepository $carTypeRepository
    )
    {
        $this->carTypeRepository = $carTypeRepository;
    }

    /**
     * @param GetFareConditionsRequest $getFareConditionsRequest
     * @return mixed
     */
    public function run(GetFareConditionsRequest $getFareConditionsRequest)
    {

        $cars = $this->carTypeRepository->all();
        $data = trans('farecalculation.farecalc');
        $data .= trans('farecalculation.init_meter') . ' 0.30';
        $data .= trans('farecalculation.baseprice') . ' ' . env('FARE_BASE_PRICE');
        if ($cars->count() > 0) {
            for ($i = 0; $i < $cars->count(); $i++)
                $data .= trans('farecalculation.' . $cars[$i]->type) . ' : ' . $cars[$i]->base_price;

        }
        return $data;


    }
}
