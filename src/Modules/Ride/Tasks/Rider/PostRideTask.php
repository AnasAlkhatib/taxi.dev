<?php

namespace Sto\Modules\Ride\Tasks\Rider;

use League\Flysystem\Config;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Requests\Rider\PostRideRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Fare\Fare;
use Sto\Services\GeoTools\GeoTools;
use Sto\Modules\User\Repositories\Criterias\Eloquent\DriverNearByCriteria;
use Sto\Services\PushService\PushService;


/**
 * Class PostRideTask
 * @package Sto\Modules\Ride\Tasks\Rider
 */
class PostRideTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;

    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var \Sto\Modules\Ride\Contracts\RideRepositoryInterface
     */
    private $rideRepository;

    /**
     * PostUserRideTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param UserRepositoryInterface $userRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
        $this->rideRepository = $rideRepository;
    }

    /**
     * @param PostRideRequest $postRideRequest
     * @return mixed
     */
    public function run(PostRideRequest $postRideRequest)
    {

        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $postRideRequest->except('payment');
        $data['user_id'] = $user->id;
        $data['ride_status_id'] = 1;
        $data['fare'] = 0.0;
        if($postRideRequest->latitude_from &&
                    $postRideRequest->longitude_from &&
                    $postRideRequest->latitude_to &&
                    $postRideRequest->longitude_to){
            $data['fare'] = Fare::fareCalculator(
                GeoTools::getDistance(
                    $postRideRequest->latitude_from,
                    $postRideRequest->longitude_from,
                    $postRideRequest->latitude_to,
                    $postRideRequest->longitude_to
                ), env('FARE_BASE_PRICE'));
        }


        $result=$this->rideRepository->create($data);

        //if ride request  is now
        if($data['ride_type']==1) {
            $users = $this->userRepository->pushCriteria(new DriverNearByCriteria($postRideRequest->latitude_from,
                $postRideRequest->longitude_from,
                10, $postRideRequest->car_type_id))->all();
           
            if ($users->count() == 0) {   // no driver near by found
                if ($user['os'] == 'Android' && !empty($user['device_token']))
                    PushService::pushSingleMessage($user['device_token'],
                                                   PushService::setCancelMessage(0,'No Drivers Near By ', 2),
                                                   'RiderAndroid');

                else if ($user['os'] == 'IOS' && !empty($user['device_token']))
                    PushService::pushSingleMessage($user['device_token'],
                                                   PushService::setCancelMessage(0, 'No Drivers Near By ', 2),
                                                   'RiderAppIOS');
            } else if ($users->count() == 1 && !empty($users[0]['device_token'])) {
                if ($users[0]['os'] == 'Android')
                    PushService::pushSingleMessage($users[0]['device_token'],
                                                   PushService::setCustomMessage($user, $result['uuid'], 'There is a ride request', 1),
                                                   'DriverAndroid');
                else if ($users[0]['os'] == 'IOS')
                    PushService::pushSingleMessage($users[0]['device_token'],
                                                   PushService::setCustomMessage($user, $result['uuid'], 'There is a ride request', 1),
                                                  'DriverAppIOS');
            } else if ($users->count() > 1)
                PushService::pushMultipleMessage($users, PushService::setCustomMessage($user, $result['uuid'], 'There is a ride request', 1));

        }
        return $result;


    }

}