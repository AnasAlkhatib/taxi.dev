<?php

namespace Sto\Modules\Ride\Tasks\Driver;


use Sto\Modules\Ride\Repositories\Criterias\Eloquent\WhereUuidAndUserId;
use Sto\Modules\Ride\Requests\Driver\PutRideStatusRequest;
use Sto\Modules\User\Repositories\Criterias\Eloquent\RiderTokenByRideIdCriteria;
use Sto\Modules\User\Repositories\Eloquent\UserRepository;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Contracts\DriverRideRepositoryInterface;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\GeoTools\GeoTools;
use Sto\Services\PushService\PushService;

class PutRideStatusTask extends Task
{

    private $authenticationService;

    private $rideRepository;

    private $driverRideRepository;

    private $userRepository;

    /**
     * PutRideStatusTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param DriverRideRepositoryInterface $driverRideRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        DriverRideRepositoryInterface $driverRideRepository,
        AuthenticationService $authenticationService,
        UserRepositoryInterface $userRepository
    )
    {
        $this->authenticationService = $authenticationService;
        $this->rideRepository = $rideRepository;
        $this->driverRideRepository = $driverRideRepository;
        $this->userRepository = $userRepository;
     }

    /**
     * @param PutRideStatusRequest $putRideStatusRequest
     * @return mixed
     */
    public function run(PutRideStatusRequest $putRideStatusRequest)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        $data = $putRideStatusRequest->all();
        $data['user_id'] = $user->id;

        if($data['ride_status_id'] !== 5){
            if ($this->rideRepository->update(['ride_status_id' => $data['ride_status_id']], $data['uuid'])) {
                //check status of the ride and send notification according to
                if ($data['ride_status_id'] == 4) {
                    //if the ride already accepted by another driver return null  
                    if ($this->driverRideRepository->findWhere(
                            [
                                ['uuid', '=', $data['uuid']],
                                ['ride_status_id', '=', 4],
                                ['user_id', '!=', $user->id]
                            ]


                        )->count() > 0
                    ) return null;


                    $rider=$this->userRepository->pushCriteria(new RiderTokenByRideIdCriteria($data['uuid']))->first();

                    $estimated_time=GeoTools::getEta( $user->latitude,
                                                      $user->longitude,
                                                      $this->rideRepository->find($data['uuid'])->latitude_from,
                                                      $this->rideRepository->find($data['uuid'])->longitude_from
                                                     );

                    if($rider['os']=='Android' && !empty($rider['device_token']) )
                        PushService::pushSingleMessage($rider['device_token'], PushService::setAcceptCustomMessage($rider->id,$data['uuid'],ceil($estimated_time),$user->name,$user->phone_number,'Driver will be in '.ceil($estimated_time) .'Minutes',3),'RiderAndroid');
                    else if ($rider['os']=='IOS' && !empty($rider['device_token']))
                        PushService::pushSingleMessage($rider['device_token'], PushService::setAcceptCustomMessage($rider->id,$data['uuid'],ceil($estimated_time),$user->name,$user->phone_number,'Driver will be in '.ceil($estimated_time) .'Minutes',3),'RiderAppIOS');


                }

                return $this
                    ->driverRideRepository
                    ->updateOrCreate(
                        [
                            'uuid' => $data['uuid'],
                            'user_id' => $data['user_id'],

                        ],
                        [
                            'ride_status_id' => $data['ride_status_id']
                        ]
                    );
            }
        }else{
            $this->driverRideRepository->pushCriteria(new WhereUuidAndUserId($user->id, $putRideStatusRequest->uuid));
            $ride = $this->driverRideRepository->all()->first();
 
            if(isset($ride->id)){
                $this->rideRepository->update(['ride_status_id' => $data['ride_status_id']], $data['uuid']);
                $this->driverRideRepository->delete($ride->id);

                //send notification to rider
                $rider=$this->userRepository->pushCriteria(new RiderTokenByRideIdCriteria($data['uuid']))->first();

                if($rider['os']=='Android' && !empty($rider['device_token']) )
                    PushService::pushSingleMessage($rider['device_token'], PushService::setCancelMessage($data['uuid'],'Ride is Rejected',5),'RiderAndroid');
                else if ($rider['os']=='IOS' && !empty($rider['device_token']))
                    PushService::pushSingleMessage($rider['device_token'], PushService::setCancelMessage($data['uuid'],'Ride is Rejected',5),'RiderAppIOS');


                return null;
            }


            return $ride;
        }
       

    }

}