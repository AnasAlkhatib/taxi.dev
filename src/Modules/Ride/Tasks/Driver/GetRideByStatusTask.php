<?php


namespace Sto\Modules\Ride\Tasks\Driver;


use Sto\Modules\Ride\Requests\Driver\GetRideByStatusRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\ThisUserCriteria;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\RideStatusCriteria;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;

class   GetRideByStatusTask extends Task
{
    private $rideRepository;
    private $authenticationService;

    /**
     * GetRideByStatusTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        AuthenticationService $authenticationService
    ) {
        $this->rideRepository = $rideRepository;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param GetRideByStatusRequest $getRideByStatusRequest
     * @return mixed
     */
    public function run(GetRideByStatusRequest $getRideByStatusRequest)
    {
        //$user = $this->authenticationService->getAuthenticatedUser();
        //$this->rideRepository->pushCriteria(new ThisUserCriteria($user->id));
        $this->rideRepository->pushCriteria(new RideStatusCriteria($getRideByStatusRequest->status));
        $this->rideRepository->pushCriteria(new OrderByCreationDateDescendingCriteria());

        $rides = $this->rideRepository->paginate();
        return $rides;
    }
}