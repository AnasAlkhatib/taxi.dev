<?php


namespace Sto\Modules\Ride\Tasks\Driver;


use Sto\Modules\Ride\Requests\Driver\GetRidesCountsByStatusRequest;
use Sto\Services\Core\Task\Abstracts\Task;
use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\ThisUserCriteria;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\RideStatusCriteria;
use Sto\Modules\Ride\Repositories\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;

class GetRidesCountsByStatusTask extends Task
{
    private $rideRepository;
    private $authenticationService;

    /**
     * GetRidesCountsByStatusTask constructor.
     * @param RideRepositoryInterface $rideRepository
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        RideRepositoryInterface $rideRepository,
        AuthenticationService $authenticationService
    ) {
        $this->rideRepository = $rideRepository;
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param GetRidesCountsByStatusRequest $getRidesCountsByStatusRequest
     * @return mixed
     */
    public function run(GetRidesCountsByStatusRequest $getRidesCountsByStatusRequest)
    {
        //$user = $this->authenticationService->getAuthenticatedUser();
        //$this->rideRepository->pushCriteria(new ThisUserCriteria($user->id));
        $this->rideRepository->pushCriteria(new RideStatusCriteria($getRidesCountsByStatusRequest->status));
        $this->rideRepository->pushCriteria(new OrderByCreationDateDescendingCriteria());

        $rides = $this->rideRepository->all();
        return count($rides);
    }
}