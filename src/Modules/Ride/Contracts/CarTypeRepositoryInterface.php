<?php

namespace Sto\Modules\Ride\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CarTypeRepositoryInterface
 * @package Sto\Modules\Ride\Contracts
 */
interface CarTypeRepositoryInterface extends RepositoryInterface
{

}
