<?php

namespace Sto\Modules\Ride\Repositories\Criterias\Eloquent;

use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ActiveRideCriteria extends Criteria
{

    private $userId;

    /**
     * ActiveRideCriteria constructor.
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @param                                                   $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->join('driver_rides', function($join)
            {
                $join->on('rides.uuid', '=', 'driver_rides.uuid')
                    ->where('driver_rides.user_id', '=', $this->userId)
                    ->where('driver_rides.ride_status_id', '<>', 5);
            });
    }

}