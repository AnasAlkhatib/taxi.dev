<?php


namespace Sto\Modules\Ride\Repositories\Criterias\Eloquent;


use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class RideStatusCriteria extends Criteria
{
    /**
     * @var int
     */
    private $status;

    /**
     * ThisUserCriteria constructor.
     *
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }
    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('ride_status_id', '=', $this->status);
    }

}