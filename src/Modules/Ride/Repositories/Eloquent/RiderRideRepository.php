<?php

namespace Sto\Modules\Ride\Repositories\Eloquent;

use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Modules\Ride\Models\RiderRide;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class RiderRideRepository
 * @package Sto\Modules\Ride\Repositories\Eloquent
 */
class RiderRideRepository extends Repository implements RideRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id'  => '=',
        'uuid'  => '=',
    ];


    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return RiderRide::class;
    }

}