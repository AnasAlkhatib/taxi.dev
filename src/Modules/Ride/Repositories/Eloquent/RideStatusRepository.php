<?php

namespace Sto\Modules\Ride\Repositories\Eloquent;

use Sto\Modules\Ride\Contracts\RideRepositoryInterface;
use Sto\Modules\Ride\Models\RideStatus;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class RideStatusRepository
 * @package Sto\Modules\Ride\Repositories\Eloquent
 */
class RideStatusRepository extends Repository implements RideRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status'  => 'like',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return RideStatus::class;
    }
    
}