<?php
/**
 * Admin Module Routes File.
 */

$router->post('login', [
    'uses' => 'LoginAdminController@handle',
]);
$router->post('register', [
    'uses' => 'RegisterAdminController@handle',
]);


$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:admin'
            ]
    ], function ($router) {

    $router->post('me/logout', ['uses' => 'LogoutAdminController@handle']);
    $router->get('me', ['uses' => 'MeAdminDetailsController@handle']);

});




