<?php

namespace Sto\Modules\Admin\Controllers\Api;

use Sto\Modules\Admin\Requests\RegisterAdminRequest;
use Sto\Modules\Admin\Tasks\CreateAdminTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class RegisterController.
 *
 *
 */
class RegisterAdminController extends ApiController
{
    /**
     * @param RegisterAdminRequest $registerRequest
     * @param CreateAdminTask $createAdminTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        RegisterAdminRequest $registerRequest,
        CreateAdminTask $createAdminTask
    ) {
        $user = $createAdminTask->run(
            $registerRequest['email'],
            $registerRequest['password'],
            $registerRequest['name'],
            $registerRequest['phone_number'],
            true
        );

        return $this->response->item($user, new UserTransformer());
    }
}
