<?php

namespace Sto\Modules\User\Seeders;

use Illuminate\Database\Seeder;
use Sto\Modules\User\Models\User;
use Illuminate\Support\Facades\Hash;

class SeedUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rider1 = New User();
        $rider1->name = 'Rider';
        $rider1->password = Hash::make(1234567);
        $rider1->email = ('rider@maued.dev');
        $rider1->phone_number = '+987654555';
        $rider1->locale = 'ar';
        $rider1->os = 'Android';
        $rider1->latitude = 32.05371890;
        $rider1->longitude = 35.89969350;
        $rider1->save();
        $rider1->attachRole(2);
        $rider1->save();

        $driver1 = New User();
        $driver1->name = 'driver1';
        $driver1->password = Hash::make(1234567);
        $driver1->email = ('driver1@maued.dev');
        $driver1->phone_number = '+987654321';
        $driver1->locale = 'ar';
        $driver1->os = 'Android';
        $driver1->latitude = 32.05410990;
        $driver1->longitude = 35.90003400;
        $driver1->save();
        $driver1->attachRole(3);
        $driver1->save();

        $driver2 = New User();
        $driver2->name = 'driver2';
        $driver2->password = Hash::make(1234567);
        $driver2->email = ('driver2@maued.dev');
        $driver2->phone_number = '+977654321';
        $driver2->locale = 'ar';
        $driver2->os = 'Android';
        $driver2->latitude = 32.05410997;
        $driver2->longitude = 35.90003407;
        $driver2->save();
        $driver2->attachRole(3);
        $driver2->save();

        $driver3 = New User();
        $driver3->name = 'driver3';
        $driver3->password = Hash::make(1234567);
        $driver3->email = ('driver3@maued.dev');
        $driver3->phone_number = '+967654321';
        $driver3->locale = 'ar';
        $driver3->os = 'IOS';
        $driver3->latitude = 32.05410993;
        $driver3->longitude = 35.90003403;
        $driver3->save();
        $driver3->attachRole(3);
        $driver3->save();

        $driver4 = New User();
        $driver4->name = 'driver4';
        $driver4->password = Hash::make(1234567);
        $driver4->email = ('driver4@maued.dev');
        $driver4->phone_number = '+957654321';
        $driver4->locale = 'ar';
        $driver4->os = 'IOS';
        $driver4->latitude = 32.05410991;
        $driver4->longitude = 35.90003401;
        $driver4->save();
        $driver4->attachRole(3);
        $driver4->save();

        /* $table->string('email')->unique();
            $table->string('phone_number', 20)->unique();
            $table->string('password',60);
            $table->string('picture_url')->nullable();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->boolean('available')->default(true);
            $table->enum('locale',['ar', 'en'])->default('ar');
            $table->boolean('is_active')->default(0);
            $table->string('device_token',255);
            $table->enum('os', ['IOS', 'Android']);*/


    }
}