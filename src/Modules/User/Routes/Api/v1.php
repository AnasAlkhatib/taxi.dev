<?php
/**
 * User Module Routes File.
 */

$router->post('login', [
    'uses' => 'LoginController@handle',
]);

$router->post('logout', [
    'uses'       => 'LogoutController@handle',
    'middleware' => [
        'api.auth',
    ],
]);

$router->post('register', [
    'uses' => 'RegisterController@handle',
]);

$router->put('users/{id}', [
    'uses'       => 'UpdateUserController@handle',
    'middleware' => [
        'api.auth',
    ],
]);

$router->delete('users/{id}', [
    'uses'       => 'DeleteUserController@handle',
    'middleware' => [
        'api.auth',
    ],
]);

$router->get('users', [
    'uses'       => 'ListAllUsersController@handle',
    'middleware' => [
        'api.auth',
        'role:admin'
    ],
]);
