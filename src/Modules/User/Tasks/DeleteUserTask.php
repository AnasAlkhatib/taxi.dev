<?php

namespace Sto\Modules\User\Tasks;

use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class DeleteUserTask.
 *
 * 
 */
class DeleteUserTask extends Task
{

    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UpdateUserTask constructor.
     *
     * @param \Sto\Modules\User\Contracts\UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    public function run($userId)
    {
        // delete the record from the users table.
        $this->userRepository->delete($userId);

        return true;
    }
}
