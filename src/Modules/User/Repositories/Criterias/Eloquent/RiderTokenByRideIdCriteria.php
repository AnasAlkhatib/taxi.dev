<?php

namespace Sto\Modules\User\Repositories\Criterias\Eloquent;

use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class RiderTokenByRideIdCriteria extends Criteria
{

    private $uuid;

    /**
     * RiderTokenByRideIdCriteria constructor.
     * @param $uuid
     */
    public function __construct($uuid)
    {
       $this->uuid = $uuid;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {

        return $model->join('rides', function($join)
        {
            $join->on('rides.user_id', '=', 'users.id')
                ->where('rides.uuid', '=', $this->uuid);
        });




    }

}