<?php

namespace Sto\Modules\User\Repositories\Criterias\Eloquent;

use Sto\Services\Core\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class DriverTokenByRideIdCriteria extends Criteria
{

    private $uuid;

    /**
     * RiderTokenByRideIdCriteria constructor.
     * @param $uuid
     */
    public function __construct($uuid)
    {
       $this->uuid = $uuid;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {

        return $model->join('driver_rides', function($join)        {
            $join->on('driver_rides.user_id', '=', 'users.id')
                ->where('driver_rides.uuid', '=', $this->uuid);
        });




    }

}