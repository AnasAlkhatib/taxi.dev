<?php

namespace Sto\Modules\User\Repositories\Eloquent;

use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Modules\User\Models\User;
use Sto\Services\Core\Repository\Abstracts\Repository;

/**
 * Class UserRepository.
 *
 * 
 */
class UserRepository extends Repository implements UserRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'  => 'like',
        'email' => '=',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}
