<?php

namespace Sto\Modules\User\Controllers\Api;

use Sto\Modules\User\Requests\UpdateUserRequest;
use Sto\Modules\User\Tasks\UpdateUserTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class UpdateUserController.
 *
 * 
 */
class UpdateUserController extends ApiController
{

    /**
     * @param \Sto\Modules\User\Requests\UpdateUserRequest $updateUserRequest
     * @param \Sto\Modules\User\Tasks\UpdateUserTask       $updateUserTask
     * @param                                               $userId
     *
     * @return \Dingo\Api\Http\Response
     */
    public function handle(UpdateUserRequest $updateUserRequest, UpdateUserTask $updateUserTask, $userId)
    {
        $user = $updateUserTask->run(
            $userId,
            $updateUserRequest['password'],
            $updateUserRequest['name']
        );

        return $this->response->item($user, new UserTransformer());
    }
}