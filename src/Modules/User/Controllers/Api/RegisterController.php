<?php

namespace Sto\Modules\User\Controllers\Api;

use Sto\Modules\User\Requests\RegisterRequest;
use Sto\Modules\User\Tasks\CreateUserTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class RegisterController.
 *
 * 
 */
class RegisterController extends ApiController
{

    /**
     * @param \Sto\Modules\User\Requests\RegisterRequest  $registerRequest
     * @param \Sto\Modules\User\Tasks\AssignUserRolesTask $assignUserRolesTask
     *
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        RegisterRequest $registerRequest,
        CreateUserTask $createUserTask
    ) {

        // create and login (true parameter) the new user
        $user = $createUserTask->run(
            $registerRequest['email'],
            $registerRequest['password'],
            $registerRequest['name'],
            $registerRequest['phone_number'],
            true
        );

        return $this->response->item($user, new UserTransformer());
    }
}
