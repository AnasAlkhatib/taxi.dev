<?php

namespace Sto\Modules\User\Controllers\Api;

use Sto\Modules\User\Requests\DeleteUserRequest;
use Sto\Modules\User\Tasks\DeleteUserTask;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class DeleteUserController.
 *
 * 
 */
class DeleteUserController extends ApiController
{

    /**
     * @param \Sto\Modules\User\Requests\DeleteUserRequest $deleteUserRequest
     * @param \Sto\Modules\User\Tasks\DeleteUserTask       $deleteUserTask
     * @param                                               $userId
     *
     * @return \Dingo\Api\Http\Response
     */
    public function handle(DeleteUserRequest $deleteUserRequest, DeleteUserTask $deleteUserTask, $userId)
    {
     
        $deleteUserTask->run($userId);

        return $this->response->accepted(null, [
            'message' => 'User (' . $userId . ') Deleted Successfully.',
        ]);
    }
}
