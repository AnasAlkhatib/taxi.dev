<?php

namespace Sto\Modules\User\Exceptions;

use Sto\Services\Core\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountFailedException.
 *
 * 
 */
class AccountFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new User.';
}
