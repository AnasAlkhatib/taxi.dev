<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Modules\Rider\Requests\LoginRiderRequest;
use Sto\Modules\Rider\Tasks\LoginRiderTask;
use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;

/**
 * Class LoginController.
 *
 * 
 */
class LoginRiderController extends ApiController
{
    /**
     * @SWG\Post(
     *      path="/riders/login",
     *      summary="Generate auth token for rider",
     *      tags={"Authentication Rider"},
     *      description="Generate authentication token based on email and password",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          default="{""email"":""rider@maued.dev"",""password"":""1234567""}",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  description="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiderSuccessResponse"
     *              ),
     *          )
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="Credentials Incorrect.",
     *     ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error",
     *     )
     * )
     */
    /**
     * @param LoginRiderRequest $loginRiderRequest
     * @param LoginRiderTask $loginTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(LoginRiderRequest $loginRiderRequest, LoginRiderTask $loginRiderTask)
    {
        $user = $loginRiderTask->run($loginRiderRequest['email'], $loginRiderRequest['password']);
        return $this->response->item($user, new UserTransformer());
    }
}
