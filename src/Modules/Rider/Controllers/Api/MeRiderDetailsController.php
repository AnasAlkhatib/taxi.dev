<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Rider\Tasks\MeRiderDetailsTask;
use Sto\Services\Core\Request\Manager\HttpRequest;
use Sto\Modules\User\Transformers\UserTransformer;

class MeRiderDetailsController extends ApiController
{
    /**
     * @SWG\Get(
     *      path="/riders/me",
     *      summary="Rider Details",
     *      tags={"Rider Account"},
     *      description="Get current rider details",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="OK.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="bad credentials.",
     *     )
     * )
     */

    /**
     * @param HttpRequest $httpRequest
     * @param MeRiderDetailsTask $me
     * @return \Dingo\Api\Http\Response
     */
    public function handle(HttpRequest $httpRequest, MeRiderDetailsTask $me)
    {
        $user = $me->run();
        return $this->response->item($user, new UserTransformer());
    }
}