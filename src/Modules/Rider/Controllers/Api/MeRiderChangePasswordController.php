<?php

namespace Sto\Modules\Rider\Controllers\Api;

use Sto\Modules\User\Transformers\UserTransformer;
use Sto\Services\Core\Controller\Abstracts\ApiController;
use Sto\Modules\Rider\Requests\MeRiderChangePasswordRequest;
use Sto\Modules\Rider\Tasks\MeRiderChangePasswordTask;

/**
 * Class MeRiderChangePasswordController
 * @package Sto\Modules\Rider\Controllers\Api
 */
class MeRiderChangePasswordController extends ApiController
{
    /**
     * @SWG\Put(
     *      path="/riders/me/settings/password",
     *      summary="Change Rider Password",
     *      tags={"Rider Account"},
     *      description="Change driver password and refresh token for user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User that should be stored",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="password",
     *                  description="password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="newPassword",
     *                  description="New Password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="newPassword_confirmation",
     *                  description="New Password Confirmation",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema (
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RiderSuccessResponse"
     *              ),
     *          )
     *      ),
     *    @SWG\Response(
     *         response="422",
     *         description="Validation Error"
     *     ),
     *    @SWG\Response(
     *         response="401",
     *         description="Authentication Failed."
     *     ),
     *    @SWG\Response(
     *         response="409",
     *         description="Update failed."
     *     )
     * )
     */

    /**
     * @param MeRiderChangePasswordRequest $changePasswordRequest
     * @param MeRiderChangePasswordTask $changePasswordTask
     * @return \Dingo\Api\Http\Response
     */
    public function handle(
        MeRiderChangePasswordRequest $changePasswordRequest,
        MeRiderChangePasswordTask $changePasswordTask
    ) {
        $result = $changePasswordTask->run($changePasswordRequest);
        return $this->response->item($result, new UserTransformer());
    }
}