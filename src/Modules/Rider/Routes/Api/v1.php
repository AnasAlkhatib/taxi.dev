<?php
/**
 * Rider Module Routes File.
 */

$router->post('login', [
    'uses' => 'LoginRiderController@handle',
]);
$router->post('register', [
    'uses' => 'RegisterRiderController@handle',
]);


$router->group(
    [
        'middleware' =>
            [
                'api.auth',
                'role:rider'
            ]
    ], function ($router) {

    $router->post('me/logout', ['uses' => 'LogoutRiderController@handle']);
    $router->get('me', ['uses' => 'MeRiderDetailsController@handle']);
    $router->put('/me/settings/password', ['uses' => 'MeRiderChangePasswordController@handle']);
    $router->put('/me/settings/basic', ['uses' => 'MeRiderUpdateBasicInfoController@handle']);
    $router->put('me/settings/locations', ['uses' => 'MeRiderUpdateLocation@handle']);
    $router->post('me/settings/picture', ['uses' => 'MeRiderUpdateProfileImage@handle']);

});




