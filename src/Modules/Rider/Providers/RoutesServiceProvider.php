<?php

namespace Sto\Modules\Rider\Providers;

use Illuminate\Routing\Router as LaravelRouter;
use Sto\Services\Core\Route\Providers\ApiRouteServiceProvider;

/**
 * Class RoutesServiceProvider.
 *
 * 
 */
class RoutesServiceProvider extends ApiRouteServiceProvider
{

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $webRouter
     */
    public function map(LaravelRouter $webRouter)
    {
        $this->apiRouter->version('v1', function ($router) {

            $router->group([
                'namespace'  => 'Sto\Modules\Rider\Controllers\Api', // Routes Namespace
                'middleware' => 'api.throttle',                      // Enable: API Rate Limiting
                'limit'      => env('API_LIMIT'),                    // The API limit time.
                'expires'    => env('API_LIMIT_EXPIRES'),             // The API limit expiry time.
                'prefix'     => 'riders'
            ], function ($router) {
                require app_path('../src/Modules/Rider/Routes/Api/v1.php');
            });

        });

        $webRouter->group([
            'namespace' => 'Sto\Modules\Rider\Controllers\Web',
        ], function ($router) {
            require app_path('../src/Modules/Rider/Routes/Web/main.php');
        });
    }
}
