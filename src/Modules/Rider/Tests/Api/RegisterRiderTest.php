<?php

namespace Sto\Modules\Rider\Tests\Api;

use Sto\Services\Core\Test\Abstracts\TestCase;

/**
 * Class RegisterEndpointTest.
 *
 * 
 */
class RegisterRiderTest extends TestCase
{

    private $endpoint = '/register';

    public function testRegisterNewRider_()
    {
        $data = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '200');

        $this->assertResponseContainKeyValue([
            'email' => $data['email'],
            'name'  => $data['name'],
        ], $response);

        // assert response contain the correct data
        $this->assertResponseContainKeys(['id', 'token'], $response);

        // assert the data is stored in the database
        $this->seeInDatabase('users', ['email' => $data['email']]);
    }

    public function testRegisterNewRiderUsingGetVerb()
    {
        $data = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'get', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '405');

        // assert response contain the correct message
        $this->assertResponseContainKeyValue([
            'message' => '405 Method Not Allowed',
        ], $response);
    }

    public function testRegisterExistingRider()
    {
        $userDetails = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
            'phone_number' => '+15915544326',
        ];

        // get the logged in user (create one if no one is logged in)
        $this->registerAndLoginTestingUser($userDetails);

        $data = [
            'email'    => $userDetails['email'],
            'name'     => $userDetails['name'],
            'password' => $userDetails['password'],
            'phone_number' => $userDetails['phone_number'],
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '409');

        // assert response contain the correct message
        $this->assertResponseContainKeyValue([
            'message' => 'Failed creating new User.',
        ], $response);
    }

    public function testRegisterNewRiderWithoutEmail()
    {
        $data = [
            'name'     => 'Sto',
            'password' => 'secret',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert response contain the correct message
        $this->assertValidationErrorContain($response, [
            'email' => 'The email field is required.',
        ]);
    }

    public function testRegisterNewRiderWithoutPhone()
    {
        $data = [
            'email'    => 'mega@mail.dev',
            'name'     => 'Sto',
            'password' => 'secret',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert response contain the correct message
        $this->assertValidationErrorContain($response, [
            'phone_number' => 'The phone number field is required.',
        ]);
    }

    public function testRegisterNewRiderWithoutName()
    {
        $data = [
            'email'    => 'mega@mail.dev',
            'password' => 'secret',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert response contain the correct message
        $this->assertValidationErrorContain($response, [
            'name' => 'The name field is required.',
        ]);
    }

    public function testRegisterNewRiderWithoutPassword()
    {
        $data = [
            'email' => 'mega@mail.dev',
            'name'  => 'Sto',
        ];

        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert response contain the correct message
        $this->assertValidationErrorContain($response, [
            'password' => 'The password field is required.',
        ]);
    }

    public function testRegisterNewRiderWithInvalidEmail()
    {
        $data = [
            'email'    => 'missing-at.dev',
            'name'     => 'Sto',
            'password' => 'secret',
        ];

        // send the HTTP request
        $response = $this->apiCall($this->endpoint, 'post', $data, false);

        // assert response status is correct
        $this->assertEquals($response->getStatusCode(), '422');

        // assert response contain the correct message
        $this->assertValidationErrorContain($response, [
            'email' => 'The email must be a valid email address.',
        ]);
    }
}
