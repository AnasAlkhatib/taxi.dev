<?php

namespace Sto\Modules\Rider\Tasks;

use Illuminate\Support\Facades\Hash;
use Sto\Modules\Rider\Exceptions\AccountUpdateException;
use Sto\Modules\Rider\Requests\MeRiderUpdateBasicInfoRequest;
use Sto\Services\Authentication\Portals\AuthenticationService;
use Sto\Modules\User\Contracts\UserRepositoryInterface;
use Sto\Services\Core\Task\Abstracts\Task;

/**
 * Class MeRiderUpdateBasicInfoTask
 * @package Sto\Modules\Rider\Tasks
 */
class MeRiderUpdateBasicInfoTask extends Task
{
    /**
     * @var \Sto\Services\Authentication\Portals\AuthenticationService
     */
    private $authenticationService;
    /**
     * @var \Sto\Modules\User\Contracts\UserRepositoryInterface
     */
    private $userRepository;

    /**
     * MeRiderChangePasswordTask constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthenticationService $authenticationService
    ) {
        $this->authenticationService = $authenticationService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param MeRiderUpdateBasicInfoRequest $request
     * @return mixed
     */
    public function run(MeRiderUpdateBasicInfoRequest $request)
    {
        $user = $this->authenticationService->getAuthenticatedUser();
        return $this->userRepository->update($request->all(),$user->id);
    }

}