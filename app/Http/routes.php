<?php

use Edujugon\PushNotification\PushNotification;

//use PushNotification;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/sendmessage', function () {
  // return Sto\Modules\Driver\Models\Car::find(1)->type;

    /*$push = new PushNotification('apn');
    $push->setMessage([
            'aps' => [
                'alert' => [
                    'title' => 'This is the title',
                    'body' => 'This is the body'
                ],
                'sound' => 'default'

            ],
            'extraPayLoad' => [
                'custom' => 'My custom data',
            ]
        ])
    ->setDevicesToken(['265cf9609e70c4b698df489adc4f86740a7a13be064c08fe478cb2657eb4c1ca']);*/
   //$push = new Edujugon\PushNotification\Facades\PushNotification('apn');

    $push = new PushNotification('apn');
    $push->setMessage([
        'aps' => [
            'alert' => [
                'title' => 'This is the title',
                'body' => 'This is the body'
            ],
            'sound' => 'default'

        ],
        'extraPayLoad' => [
            'custom' => 'My custom data',
        ]
    ])
        ->setDevicesToken('265cf9609e70c4b698df489adc4f86740a7a13be064c08fe478cb2657eb4c1ca')
        ->send()
    ->getFeedback();

    if(isset($push->feedback->error)){
       dd($push->feedback->error);
    }
    dd($push);
  /*  Edujugon\PushNotification\Facades\PushNotification::setService('apn')
        ->setMessage([
            'notification' => [
                'title'=>'This is the title',
                'body'=>'This is the message',
                'sound' => 'default'
            ],
            'data' => [
                'extraPayLoad1' => 'value1',
                'extraPayLoad2' => 'value2'
            ]
        ])
        //->setApiKey('Server-API-Key')
        ->setDevicesToken(['265cf9609e70c4b698df489adc4f86740a7a13be064c08fe478cb2657eb4c1ca'])
                        ->send()
        ->getFeedback();

   // dd('message_send');*/

});
