<?php

namespace Sto\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Sto\Modules\Ride\Models\Ride;
use Sto\Modules\User\Models\User;
use Carbon\Carbon;
use Sto\Services\PushService\PushService;

class NotifyScheduledRides extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to scheduled rides.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("*******************************************" . PHP_EOL);
        $this->comment("** Job Started on " . Carbon::now()      . PHP_EOL);
        $this->comment("*******************************************" . PHP_EOL);
        $collection = Ride::whereBetween('pickup_time', [Carbon::now(), Carbon::now()->addMinutes(15)])
            ->where('ride_type', '=', 2)
            ->get();
        foreach ($collection as $item){
            //Rider side
            $user = User::find($item->user_id);
            $this->comment("Ride_id:" . $item->uuid . " and user_id: ". $user->id . PHP_EOL);
            $app = ($user->os == "Android") ?  "RiderAndroid" : "RiderAppIOS";
            if($user->device_token && $user->os){
                PushService::pushSingleMessage(
                    $user->device_token,
                    PushService::setCustomMessage($user, $item->uuid, 'There is a ride in the way.', 6),
                    $app
                );
            }

            //Driver side
            $drivers = User::filterByLocationAndDistance($item->latitude_from, $item->longitude_from, 100)->whereExists(function ($query) {
                $query->select(DB::raw("*"))
                    ->from("roles")
                    ->join("role_user", "roles.id" ,"=", "role_user.role_id")
                    ->whereRaw("role_user.user_id = users.id")
                    ->whereRaw("name = 'driver'");
            })->get();
            if($drivers->count() > 0){
                PushService::pushMultipleMessage($drivers, PushService::setCustomMessage($user, $item->uuid, 'There is a ride in the way.', 1));
                $this->comment("Ride_id:" . $item->uuid . " sent to drivers" . PHP_EOL);
            }else{
                PushService::pushSingleMessage(
                    $user->device_token,
                    PushService::setCancelMessage($item->uuid, 'No Drivers Found.', 2),
                    $app
                );
                $this->comment("Ride_id:" . $item->uuid . " No Driver Message" . PHP_EOL);
            }


        }


    }
}
